let arr = require('./data.json');

const fs = require('fs');

var options = [];

for(let a in arr["86"]){
    let province = arr["86"][a]; // 当前省
    // console.log(province); 
    let obj = {
        value: province,
        label: province,
        code: a,
        children:[],
    };
    // console.log(arr[a]); // 市的集合
    let ps = arr[a];
    for(let b in ps){
        // if(!arr[b]){
        //     break;
        // }
        let city = ps[b]; 
        if(!city){ //如果没有当前的市则跳出循环
            break;
        }
        // console.log(city) // 当前市
        let ct = {
            value: city,
            label: city,
            code: b,
            children:[],
        }
        var citys = arr[b]; //当前市的区
        // console.log(citys); // 区的集合
        for(let c in citys){  // 遍历区
            let country = citys[c];
            // if(!country) {
            //     break
            // }; //如果没有当前的市则跳出循环

            ct.children.push({
                value: country,
                label: country,
                code: c,
            });
        }
        // console.log(city,province);
        if(ct.children.length < 1){
            delete ct['children'];
        }
        //把市区和他的区集放到obj内
        obj.children.push(ct);
        console.log(obj);
    }

    options.push(obj);
}

const content = JSON.stringify(options);
console.log(content);

fs.writeFile('./city.json', content, err => {
  if (err) {
    console.error(err)
    return
  }
  //文件写入成功。
})